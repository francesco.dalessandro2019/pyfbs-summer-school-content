{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<style>.container { width:100% !important; }</style>"
      ],
      "text/plain": [
       "<IPython.core.display.HTML object>"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "from IPython.core.display import display, HTML\n",
    "display(HTML(\"<style>.container { width:100% !important; }</style>\"))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "<p>\n",
    "<img src=\"./pics/header_new.svg\" style=\"float: center\" width = \"3000px\"/>\n",
    "</p>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "source": [
    "***"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "# A3 - Python through a DS example: part 1\n",
    "<img src=\"./pics/gorenje_logo.svg\" width=\"500\" height=\"100\" align=\"right\"/>\n",
    "\n",
    "> Tomaž Bregar $\\,\\,$ *tomaz.bregar@gorenje.com* <br>\n",
    "\n",
    "\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "source": [
    "***"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "> ### <i class=\"fa fa-graduation-cap\" aria-hidden=\"true\"></i> Lesson highlights  \n",
    "> - #### Derivation of mass and stiffness matrices for a simple discrete system.\n",
    "> - #### Solving eigenvalue problem.\n",
    "> - #### Synthetization of frequency response functions (FRFs)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "source": [
    "***"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## A3.1 Introduction"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "The dynamic properties of the system are described by the equilibrium equation, where the external forces are equal to the internal forces resulting from inertia, damping, and elasticity. \n",
    "The basic equation in a time domain for a linear dynamic system takes the following form (damping is neglected):"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "$$\n",
    "\\mathbf{M} \\, \\boldsymbol{\\ddot{x}}(t) + \\mathbf{K} \\, \\boldsymbol{x}(t) = \\boldsymbol{f}(t),\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "where $\\mathbf{M}$ represents the mass matrix, $\\mathbf{K}$ the stiffness matrix, $\\boldsymbol{x}$ vector of responses at all $n$ degrees of freedom (DoFs), and $\\boldsymbol{f}$ vector of externally applied forces."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "For consistent modeling of dynamic systems within the frequency domain the following assumptions have to be valid:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "* __Linearity__ - response amplitude is linearly proportional to the excitation amplitude."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "* __Time invariance__ - mass, stiffness, and damping characteristics are time-independent."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "* __Passivity__ - the energy flow in the system is always positive or equal to zero."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "* __Reciprocity__ - the response of the structure remains the same if the excitation and response location are switched."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "* __Stability__ - the response of the system is bounded if the excitation of the system is bounded."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "If we apply the Fourier transform, a time function of response $\\boldsymbol{x}(t)$ can be written in  the frequency domain ($\\boldsymbol{x}(\\omega)$) and basic equation takes the following form: "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "$$\n",
    "\\mathbf{M} \\, \\boldsymbol{\\ddot{x}}(\\omega) + \\mathbf{K} \\, \\boldsymbol{x}(\\omega) = \\boldsymbol{f}(\\omega).\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "By following the fact that $\\boldsymbol{\\ddot{x}}(\\omega) = - \\omega^2 \\boldsymbol{x}(\\omega) $ we can express $\\boldsymbol{x}(\\omega)$ on the right-hand side: "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "$$\n",
    "(- \\omega^2\\,\\mathbf{M} + \\mathbf{K}) \\boldsymbol{x}(\\omega) = \\boldsymbol{f}(\\omega).\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "The left-hand side can be collected into a single frequency-dependent matrix $\\mathbf{Z}(\\omega)$:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "$$\n",
    "\\mathbf{Z}(\\omega) \\boldsymbol{x}(\\omega) = \\boldsymbol{f}(\\omega).\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "The impedance matrix $\\mathbf{Z}(\\omega)$ is also called the dynamic stiffness matrix (often referred to as mechanical impedance or apparent mass). By inverting the mechanical impedance an admittance notation $\\mathbf{Y}(\\omega)$ can be obtained:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "$$\n",
    "\\boldsymbol{u}(\\omega) = \\mathbf{Y}(\\omega) \\boldsymbol{f}(\\omega), \\qquad \\mathbf{Y}(\\omega) = \\left(\\mathbf{Z}(\\omega)\\right)^{-1}.\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "The $\\mathbf{Y}(\\omega)$ denotes the frequency response function matrix and is often referred\n",
    "to as admittance or dynamic flexibility. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "The admittance matrix is sometimes also denoted with the letter $\\mathbf{H}$. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Admittance matrix $\\mathbf{Y}$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "An admittance $\\mathbf{Y}_{ij}$ is defined as the response at $i$-th DoF when a unit excitation force is applied at $j$-th DoF. A whole $j$-th column from the admittance matrix $\\mathbf{Y}$ can be determined from a single excitation point; therefore, the admittance matrix is fully coupled. A single admittance FRF is a _global_ observation of the system dynamics."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "<c>\n",
    "<center><img src=\"./pics/YY_matrix.svg\"/ width = 400></center>\n",
    "</c>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Impedance matrix $\\mathbf{Z}$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "The impedance $\\mathbf{Z}_{ji}$ is defined as the force at $j$-th DoF when a unitary displacement is imposed at $i$-th DoF, while all remaining DoFs are fixed. Therefore, the impedance matrix $\\mathbf{Z}$ is commonly sparse in the sense that a single impedance term is a _local_ observation of the system dynamics."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "<c>\n",
    "<center><img src=\"./pics/ZZ_matrix.svg?123\" width = 350></center>\n",
    "</c>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## A3.2 Eigenvalue problem"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "The eigenfrequencies and eigenvectors  can be determined from the equilibrium equation, when considering a system response with free vibrations:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "$$\n",
    "\\mathbf{M}\\,\\boldsymbol{\\ddot{x}}(t)+\\mathbf{K}\\,\\boldsymbol{x}(t)=\\boldsymbol{0}.\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "Euler's identity represents the solution of the differential equation: $\\boldsymbol{x}(t)=\\boldsymbol{X}\\,e^{\\text{i}\\,\\omega\\,t}$ and\n",
    "$\\boldsymbol{\\ddot{x}}(t)=-\\omega^2\\boldsymbol{X}\\,e^{\\text{i}\\,\\omega\\,t}$.\n",
    "By transformation to modal domain the equation of motion takes the following form: $\\mathbf{M}(-\\omega^2\\boldsymbol{X}\\,e^{\\text{i}\\,\\omega\\,t})+\\mathbf{K}\\,(\\boldsymbol{X}\\,e^{\\text{i}\\,\\omega\\,t})=\\boldsymbol{0}$.\n",
    "By knowing $e^{\\text{i}\\,\\omega\\,t}\\neq 0$ for any time $t$ we obtain: "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "$$\n",
    "(\\mathbf{K} - \\omega^2\\,\\mathbf{M}) \\boldsymbol{X} = \\boldsymbol{0}.\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "To get a non-trivial solution it is necessary to satisfy $\\text{det}(\\mathbf{K}-\\omega_r^2\\,\\mathbf{M})=\\boldsymbol{0}$.\n",
    "By solving the determinant, the eigenvalues ($\\omega_1^2, \\omega_2^2, \\dots$) are determined, which represent undamped natural frequencies. \n",
    "Each eigenvalue results in corresponding eigenvector ($\\boldsymbol{\\psi}_1, \\boldsymbol{\\psi}_2, \\dots$) which represents mode shape.\n",
    "Eigenvalues and eigenvectors can be organized into matrix form:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "$$\n",
    "[^{\\nwarrow}{\\pmb{\\omega}_{r}^2}_{\\searrow}]=\n",
    "\\begin{bmatrix} \n",
    "   \\omega_1^2 & 0 & \\cdots & 0 \\\\\n",
    "   0 & \\omega_2^2 & \\cdots & 0 \\\\\n",
    "   \\vdots & \\vdots & \\ddots & \\vdots \\\\\n",
    "   0 & 0 & \\cdots & \\omega_N^2\n",
    "\\end{bmatrix}, \n",
    "   \\qquad\n",
    "   [\\pmb{\\Psi}] = \\begin{bmatrix} \\boldsymbol{\\psi}_1 & \\boldsymbol{\\psi}_2 & \\cdots & \\boldsymbol{\\psi}_N\\end{bmatrix}.\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "Modal mass and modal stiffness are calculated using the following equations:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "$$\n",
    "\\pmb{\\Psi}^{\\text{T}}\\,\\mathbf{M}\\,\\pmb{\\Psi} = [^{\\nwarrow}{m_{r}}_{\\searrow}], \n",
    "\\qquad\n",
    "\\pmb{\\Psi}^{\\text{T}}\\,\\mathbf{K}\\,\\pmb{\\Psi} = [^{\\nwarrow}{k_{r}}_{\\searrow}].\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "Mass normalised modes are calculated using equations: $\\boldsymbol{\\phi}_r=\\boldsymbol{\\psi}_r\\,\\frac{1}{\\sqrt{m_{r}}}$\n",
    "and have the following properties:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "$$\n",
    "\\pmb{\\Phi}^{\\text{T}}\\,\\mathbf{M}\\,\\pmb{\\Phi} = [\\mathbf{I}], \n",
    "   \\qquad\n",
    "   \\pmb{\\Phi}^{\\text{T}}\\,\\mathbf{K}\\,\\pmb{\\Phi} = [^{\\nwarrow}{\\pmb{\\omega}_{r}^2}_{\\searrow}].\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## A3.3 Simple discrete mass-spring system "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's first import some packages, which we will use for the calculations."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import pandas as pd\n",
    "import matplotlib.pyplot as plt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Consider a simple discrete dynamic system with 4 degrees of freedom:\n",
    "\n",
    "<p>\n",
    "<img src=\"./pics/A_.svg\"/ width = 400>\n",
    "</p>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let's define the mass matrix:\n",
    "\n",
    "$\n",
    "\\textbf{M} = \\begin{bmatrix}\n",
    "m_1 & 0 & 0 & 0 \\\\ \n",
    "0 & m_2 & 0 & 0 \\\\ \n",
    "0 & 0 & m_3 & 0 \\\\ \n",
    "0 & 0 & 0 & m_4 \\\\ \n",
    "\\end{bmatrix}\n",
    "$\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "m1 = 1\n",
    "m2 = 1\n",
    "m3 = 1\n",
    "m4 = 1\n",
    "\n",
    "m = np.array([m1,m2,m3,m4])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And ckeck out how it looks in <code>Python</code>:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "ename": "SyntaxError",
     "evalue": "invalid syntax (<ipython-input-4-922637044b41>, line 1)",
     "output_type": "error",
     "traceback": [
      "\u001b[1;36m  File \u001b[1;32m\"<ipython-input-4-922637044b41>\"\u001b[1;36m, line \u001b[1;32m1\u001b[0m\n\u001b[1;33m    M =\u001b[0m\n\u001b[1;37m       ^\u001b[0m\n\u001b[1;31mSyntaxError\u001b[0m\u001b[1;31m:\u001b[0m invalid syntax\n"
     ]
    }
   ],
   "source": [
    "M =\n",
    "\n",
    "#display(M)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can do that waaay faster:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "M = np.diag(m).astype(np.float64)\n",
    "display(M)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let's define the stiffness matrix:\n",
    "\n",
    "$\n",
    "\\textbf{K} = \\begin{bmatrix}\n",
    "k_1 & -k_1 & 0 & 0 \\\\ \n",
    "-k_1 & k_1+k_2 & -k_2 & 0 \\\\ \n",
    "0 & -k_2 & k_2+k_3  & - k_3 \\\\ \n",
    "0 & 0 & -k_3 & k_3 \\\\ \n",
    "\\end{bmatrix}\n",
    "$\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "k1 = 200\n",
    "k2 = 200\n",
    "k3 = 200\n",
    "\n",
    "k = np.array([k1,k2,k3])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "K = np.asarray([[k1,-k1,0,0],\n",
    "                [-k1,k1+k2,-k2,0],\n",
    "                [0,-k2,k2+k3,-k3],\n",
    "                [0,0,-k3,k3]])\n",
    "\n",
    "display(K)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can also define a function, that can generate the stiffness matrix of a dynamic system with an arbitrary number of degrees of freedom. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Could be to tough - \n",
    "def mdof_K_C(p):\n",
    "    N = len(p) +  1\n",
    "    mat = np.zeros((N, N))\n",
    "    for i in range(N):\n",
    "        if i == 0:\n",
    "            mat[i, :2] = np.array([ p[0] , -p[0] ]) \n",
    "        elif i == N-1:\n",
    "            mat[i, -2:] = np.array([ -p[-1], + p[-1] ])\n",
    "        \n",
    "        \n",
    "    return mat.astype(np.float64)\n",
    "\n",
    "#K = mdof_K_C(k)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-block alert-info\">\n",
    "    <i class=\"fa fa-info-circle\" aria-hidden=\"true\"></i>\n",
    "    <b>FBS TopTip:</b> Working with analytical examples can be highly beneficial when deriving new methods.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## A3.4 Solving eigenvalue problem"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As the mass and stiffness matrices of the system are now defined, we can solve the eigenvalue problem. Some additional packages are required for that:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import scipy as sp\n",
    "from scipy.sparse import linalg,diags"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "eig_val, eig_vec = sp.linalg.eigh(K, M)\n",
    "\n",
    "eig_val.sort()\n",
    "eig_freq = np.sqrt(np.abs(np.real(eig_val)))  "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-block alert-danger\">\n",
    "    <i class=\"fa fa-exclamation\" aria-hidden=\"true\"></i>\n",
    "    <b>Alert!</b> Take care with a number of eigenvalues. If you have a large number of DoFs the calculation may take quite a long time.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## A3.5 FRF generation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We will display the generation of frequency response functions in two different ways. First is the direct harmonic or the full-space method and second is the mode-superposition. Each of them has its own set of advantages and disadvantages."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Direct harmonic"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "FRFs for the small systems can be efficiently computed using the Direct harmonic method:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "$$\\left(-\\mathbf{M} \\, \\omega^2+ \\mathbf{K}\\right) \\, \\boldsymbol{X}\\,e^{\\text{i}\\,\\omega\\,t} = \\boldsymbol{F}\\,e^{\\text{i}\\,\\omega\\,t},$$\n",
    "$$\\left(-\\mathbf{M} \\, \\omega^2+ \\mathbf{K}\\right) \\, \\boldsymbol{X} = \\boldsymbol{F}.$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So, the FRF matrix can be calculated as follows:\n",
    "\n",
    "$$\\boldsymbol{Y}(\\omega) =\\frac{\\boldsymbol{X}(\\omega)}{\\boldsymbol{F}(\\omega)} = (\\mathbf{K}-\\omega^2\\,\\mathbf{M})^{-1}.$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Before generating the FRFs we first have to define the frequency vector:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "f_start = 0.01\n",
    "f_end = 5\n",
    "f_resolution = .01\n",
    "freq = np.arange(f_start, f_end, f_resolution)\n",
    "\n",
    "ome = 2 * np.pi * freq"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "Using this formulation, FRFs are generated at all DoFs included in the mass and stiffness matrices. This can cause problems in the case of bigger models with a higher number of DoFs. Hence, computations can be very time and memory-consuming.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "FRF_1 = np.zeros((4,4,len(freq)))\n",
    "\n",
    "for i,ome_i in enumerate(ome):\n",
    "    FRF_1[:,:,i] = np.linalg.inv(K-ome_i**2*M)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.semilogy(freq,np.abs(FRF_1[0,1,:]));"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Mode superposition"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For FRF generation we can also use the modal superposition method, where contributions of modes are superposed at each frequency line using the equation: "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "$$\n",
    "\\alpha_{i, j} = \\sum_{r=1}^{m}\\frac{\\boldsymbol{\\phi}_{i,r}\\,\\boldsymbol{\\phi}_{j,r}}{\\omega_r^2-\\omega^2}.\n",
    "$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "no_modes = len(eig_val)\n",
    "FRF_2 = np.zeros((no_modes, no_modes, len(freq)), dtype=complex)\n",
    "\n",
    "ome = 2 * np.pi * freq\n",
    "\n",
    "# loop for each input\n",
    "for i in range(no_modes):\n",
    "    \n",
    "    # loop for each output\n",
    "    for j in range(no_modes):\n",
    "        \n",
    "        # loop for each modeshape\n",
    "        for no in range(eig_vec.shape[1]):\n",
    "            FRF_2[i, j] += eig_vec[i, no] * eig_vec[j, no] / (-ome ** 2 + eig_freq[no] ** 2)\n",
    "            "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.semilogy(freq,np.abs(FRF_2[0,1,:]));"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we can compare the two FRF synthetizations methods:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.semilogy(freq,np.abs(FRF_1[0,1,:]))\n",
    "plt.semilogy(freq,np.abs(FRF_2[0,1,:]),'--');"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-block alert-warning\">\n",
    "    <i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i>\n",
    "    <b>Warning!</b> Even if you are interested in a small frequency region only, you should always include eigenvalues from outside this region to account for upper- and lower-residuals.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-block alert-success\">\n",
    "    <i class=\"fa fa-thumbs-up\" aria-hidden=\"true\"></i>\n",
    "    <b>Success!</b> Now we have FRFs for the simple discrete mass system.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> <i class=\"fa fa-quote-right\"></i> <b>That's a wrap! Want to know more?</b> \n",
    "> <ul>\n",
    "    <li> e Silva, Júlio M. Montalvão, and Nuno MM Maia, eds. Modal analysis and testing. Vol. 363. Springer Science & Business Media, 2012.</li>\n",
    "    <li> Tiso, P., Allen, M. S., Rixen, D., Abrahamsson, T., Van der Seijs, M., Mayes, R. L. (2020) Substructuring in Engineering Dynamics - Emerging Numerical and Experimental Techniques. Springer </a>.\n",
    "    <li>van der Seijs, M. V. (2016) Experimental dynamic substructuring: Analysis and design strategies for vehicle development. Delft University of Technology, <a href=\"https://repository.tudelft.nl/islandora/object/uuid:28b31294-8d53-49eb-b108-284b63edf670?collection=research\">10.4233/uuid:28b31294-8d53-49eb-b108-284b63edf670</a>.</li>\n",
    "</li>\n",
    "</ul>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
