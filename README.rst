Content for pyFBS Summer School
-------------------------------

This repository contains all the presentations and templates for the pyFBS Summer School. Please download the repository with the following command::

	git clone https://gitlab.com/pyFBS/pyfbs-summer-school-content.git
	
You should have Git installed to be able to download the content, alternatively you can download it as a .zip file directly from GitLab.